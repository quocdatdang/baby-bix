<?php

/**
 * Edit Product Summary Box
 * @see woocommerce_template_single_title()
 * @see woocommerce_template_single_content()
 * @see woocommerce_template_single_price()
 * @see woocommerce_template_single_sharing()
 * @see woocommerce_template_single_meta()
 * @see woocommerce_template_single_add_to_cart()
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_content', 9 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 11 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 100 );

if ( ! function_exists( 'woocommerce_template_single_content' ) ) {
	/**
	 * Output the product title.
	 *
	 * @subpackage	Product
	 */
	function woocommerce_template_single_content() {
		wc_get_template( 'single-product/the-content.php' );
	}
}
