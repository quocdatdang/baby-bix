<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package storefront
 */

get_header(); ?>

<div class="main-slider">
	<ul class="cp-child-slider">
		<?php
		$args = array( 'post_type' => 'slider', 'posts_per_page' => 3 );
		$loop = new WP_Query( $args );
		while($loop->have_posts()){
			$loop->the_post();
			$meta = get_post_meta(get_the_ID());
			$images_array = rwmb_meta( 'slider-url', 'type=image' ); 
			foreach ($images_array as $key => $image) {?>
			<li><img src=<?= $image['full_url'] ?>  alt="" />
				<div class="caption">
					<div class="container">
						<div class="slider-data">
							<h2><p><?= $meta['slogan'][0] ?></p></h2>
							<a class="shopping-button" href="/babybix/shop">Shop Now</a> </div>
						</div>
					</div>
				</li>
				<?php } }
				?>
			</ul>
		</div>
		<!--Main  Slider End--> 

		<!--Main Content Start-->
		<div class="cp-page-content"> 

			<!--Home Welcome Section Start-->
			<div class="cp-home-welcome">
				<div class="container">
					<div class="row">
						<?php $the_query = new WP_Query( 'p=13' );

										// The Loop
						if ( $the_query->have_posts() ) {
							while ( $the_query->have_posts() ) {
								$the_query->the_post();
								?>
								<h2 class="sec-title"><?php echo get_the_title(); ?></h2>
								<div class="col-md-4"><img src="<?php bloginfo("template_url")?>/images/welcomepic.jpg" alt=""></div>
								<div class="col-md-4">
									<div class="welcome-content">
										<?php echo get_the_excerpt();?>
										<a href="<?php the_permalink(); ?>" class="readmore-bg">Đọc tiếp</a> </div>
									</div>
									<div class="col-md-4">
										<div class="emergency-call">
											<h2>(08)3-762-0872</h2>
											<strong>Hotline</strong> </div>
										</div>
										<?php	}} else {
											echo 'Chưa tồn tại post giới thiệu';
										}
										/* Restore original Post Data */
										wp_reset_postdata();?>
									</div>
								</div>
							</div>
							<!--Home Welcome Section End--> 

							<!--Home Services Start-->
							<div class="home-services gap-80">
								<div class="container">
									<div class="row">
										<h2 class="sec-title">Our Services</h2>
										<div class="col-md-3">
											<div class="service-box">
												<div class="sicon"><i class="fa fa-bell-o"></i></div>
												<h3 class="sub-title">Big Goal</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
												<a href="#" class="readmore">Read More</a> </div>
											</div>
											<div class="col-md-3">
												<div class="service-box">
													<div class="sicon"><i class="fa fa-gift"></i></div>
													<h3 class="sub-title">Art Day</h3>
													<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
													<a href="#" class="readmore">Read More</a> </div>
												</div>
												<div class="col-md-3">
													<div class="service-box">
														<div class="sicon"><i class="fa fa-gears"></i></div>
														<h3 class="sub-title">Emergency Care</h3>
														<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
														<a href="#" class="readmore">Read More</a> </div>
													</div>
													<div class="col-md-3">
														<div class="service-box">
															<div class="sicon"><i class="fa fa-heart-o"></i></div>
															<h3 class="sub-title">Activities</h3>
															<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
															<a href="#" class="readmore">Read More</a> </div>
														</div>
													</div>
												</div>
											</div>
											<!--Home Services End--> 

											<!--Home Paralx Start-->
											<div class="cp-home-peralax">
												<div class="container">
													<div class="row">
														<div class="col-md-12">
															<div class="caring-content">
																<h2>Caring For Children is not just Part of what we Do. Its’s all we Do.</h2>
																<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nibh neque, convallis ut interdum a, conasequat sit amet mauris. </p>
																<a class="readmore" href="#">Support Children</a> </div>
															</div>
														</div>
													</div>
												</div>
												<!--Home Paralx End--> 

												<!--CARING CHILDREN START-->
												<div class="cp-caring-product gap-80">
													<div class="products-tabs">
														<h2 class="sec-title">Our Products</h2>
														<div role="tabpanel"> 
															<div class="container">
																<ul class="nav nav-tabs" role="tablist">

																	<!-- Nav tabs -->
																	<?php
																	$prod_cat_args = array(
  																		'taxonomy'     => 'product_cat', //woocommerce
  																		'orderby'      => 'name',
  																		'empty'        => 0
  																		);

																	$woo_categories = get_categories( $prod_cat_args );
																	$a=0;
																	foreach ( $woo_categories as $woo_cat ) {
																		$a=$a+1;
    																	$woo_cat_id = $woo_cat->term_id; //category ID
																	    $woo_cat_name = $woo_cat->name; //category name
																	    $woo_cat_slug = $woo_cat->slug; //category slug																	


																	    if($a==1){
																	    	?>
																	    	<li role="presentation" class="active"><a href="#<?php echo $woo_cat_slug;  ?>" aria-controls="<?php echo $woo_cat_slug;  ?>" role="tab" data-toggle="tab"><?php echo $woo_cat_name;  ?></a></li>
																	    	<?php														
																	}//end of $woo_categories foreach  																	
																	else{
																		?>
																		<li role="presentation"><a href="#<?php echo $woo_cat_slug;  ?>" aria-controls="<?php echo $woo_cat_slug;  ?>" role="tab" data-toggle="tab"><?php echo $woo_cat_name;  ?></a></li>

																		<?php
																	}}
																	?>
																</ul>
															</div>
															<div class="tab-content product-listing">
															<!-- Tab panes -->
															<?php
																$prod_cat_args = array(
  																		'taxonomy'     => 'product_cat', //woocommerce
  																		'orderby'      => 'name',
  																		'empty'        => 0
  																		);
																$i=0;
																$woo_categories = get_categories( $prod_cat_args );
																foreach ( $woo_categories as $woo_cat ) {
																	$i=$i+1;
    																	$woo_cat_id = $woo_cat->term_id; //category ID
																	    $woo_cat_name = $woo_cat->name; //category name
																	    $woo_cat_slug = $woo_cat->slug; //category slug																	
																	    if($i==1){?>
																	    
																<div role="tabpanel" class="tab-pane active" id="<?php echo $woo_cat_slug;  ?>">
																	    		<div class="container">
																	    			<ul class="cp-product-list row">
																
																	    <?php 
																	    	$args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => $woo_cat_name, 'orderby' => 'rand' );
																	    	$loop = new WP_Query( $args );
																	    	while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>


																	    	
																	    				<li  class="col-md-3 col-sm-6" >
																	    					<a href="<?php the_permalink(); ?>">
																	    						<div class="pro-list">
																	    							<div class="saletag">Sale</div>
																	    							<div class="thumb">
																	    								<a href="<?php the_permalink(); ?>">
																	    									<?php
																	    									if ( has_post_thumbnail() ) {
																	    										?>
																	    										<img src="<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
																	    										<?php
																	    									} else{
																	    										?>
																	    										<img src="/image/default?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
																	    										<?php
																	    									}
																	    									?>
																	    								</a>
																	    							</div>
																	    							<div class="text">
																	    								<div class="pro-name">
																	    									<a href="<?php the_permalink(); ?>">
																	    										<h4><?php the_title(); ?></h4>
																	    										<p class="price"><?=$product->get_price_html(); ?></p>
																	    									</a>
																	    								</div>
																	    								<p> <?php $excerpt = get_the_excerpt() ; 
																	    								if (strlen($excerpt) > 50) {
																	    									echo substr ($excerpt, 0,50).'....';
																	    								}else{
																	    									echo $excerpt;
																	    								}
																	    								?>
																	    							</p>
																	    						</div>
																	    						<div class="cart-options">
																	    							<!-- <a href="#"> <i class="fa fa-heart-o"> </i>Wishlist</a> -->
																	    							<?php do_action( 'woocommerce_after_shop_loop_item' );?>

																	    							<a href="<?php the_permalink(); ?>"><i class="fa fa-search"></i>Details</a> 
																	    							<a href="http://www.addthis.com/bookmark.php"class="addthis_button" style="text-decoration:none;"><i class="fa fa-share-alt"></i>Share
																	    							</a>
																	    						</div>
																	    					</div>
																	    				</a>
																	    			</li>
																	    		
																	    <?php														
																	    endwhile;?>
																	    </ul>
																	    	</div>
																	    </div>
																	   <?php    
																	}
																	else{
																		?>
																		<div role="tabpanel" class="tab-pane" id="<?php echo $woo_cat_slug;  ?>">
																	    		<div class="container">
																	    			<ul class="cp-product-list row">
																		<?php
																		$args = array( 'post_type' => 'product', 'posts_per_page' => 4, 'product_cat' => $woo_cat_name, 'orderby' => 'rand' );
																		$loop = new WP_Query( $args );
																		while ( $loop->have_posts() ) : $loop->the_post(); global $product;
																		?>
																		
																
																
																					<li  class="col-md-3 col-sm-6" >
																						<a href="<?php the_permalink(); ?>">
																							<div class="pro-list">
																								<div class="saletag">Sale</div>
																								<div class="thumb">
																									<a href="<?php the_permalink(); ?>">
																										<?php
																										if ( has_post_thumbnail() ) {
																											?>
																											<img src="<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
																											<?php
																										} else{
																											?>
																											<img src="/image/default?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
																											<?php
																										}
																										?>
																									</a>
																								</div>
																								<div class="text">
																									<div class="pro-name">
																										<a href="<?php the_permalink(); ?>">
																											<h4><?php the_title(); ?></h4>
																											<p class="price"><?=$product->get_price_html(); ?></p>
																										</a>
																									</div>
																									<p> <?php $excerpt = get_the_excerpt() ; 
																									if (strlen($excerpt) > 50) {
																										echo substr ($excerpt, 0,50).'....';
																									}else{
																										echo $excerpt;
																									}
																									?>
																								</p>
																							</div>
																							<div class="cart-options">
																								<!-- <a href="#"> <i class="fa fa-heart-o"> </i>Wishlist</a> -->
																								<?php do_action( 'woocommerce_after_shop_loop_item' );?>

																								<a href="<?php the_permalink(); ?>"><i class="fa fa-search"></i>Details</a> 
																								<a href="http://www.addthis.com/bookmark.php"class="addthis_button" style="text-decoration:none;"><i class="fa fa-share-alt"></i>Share
																								</a>
																							</div>
																						</div>
																					</a>
																				</li>

																	<?php
																	endwhile;	
																	?></ul>
																																				
																	    	</div>
																	    </div>
																	<?php }
																	}//end of $woo_categories foreach  																	

																	?>
																	
																</div>
															</div>
														</div>
														<div class="va">
														<a href="shop" class="viewallindext"><button>View All</button></a>
													</div>
													</div>
													<!--CARING CHILDREN End--> 


													<!-- Home Staff Start -->
													<div class="cp-our-staff">
														<div class="container">
															<div class="row">
																<div class="col-md-12">
																	<h2 class="sec-title">Nhân Sự</h2>
																</div>
																<ul class="cp-team-grid">
																	<?php $new_query = new WP_Query( 'post_type=teams' );
																	?>
																	<?php 
																	if ( $new_query->have_posts() ) : 
																		$thumbnail=false;
																	while( $new_query->have_posts() ) :
																		$new_query->the_post();
																	if ( function_exists('has_post_thumbnail') && has_post_thumbnail($post->ID) ) :
																		$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), full );
																	else :
																		$thumbnail=false;
																	endif; ?>

																	<li class="col-md-3 col-sm-6">
																		<div class="staff-thumb-holder"><img src="<?php echo $thumbnail[0]; ?>" alt=""></div>
																		<div class="staff-content">
																			<h4><?php the_title();?></h4>
																			<p><?php the_content(); ?></p>
																		</div>
																	</li>
																	<?php
																	endwhile;
																	endif;
																	?>
																	<!-- -->
																</ul>
															</div>
														</div>
													</div>

													<!-- Home Staff End --> 

													<!--Home Latest News Start-->
													<div class="cp-home-latest-news gap-80">
														<div class="container">
															<div class="row">
																<div class="col-md-12">
																	<h2 class="sec-title">Latest News</h2>
																</div>
																<?php $new_query = new WP_Query( 'post_type=blogs' );
																?>
																<?php 
																if ( $new_query->have_posts() ) : 
																	$thumbnail=false;
																while( $new_query->have_posts() ) :
																	$new_query->the_post();
																if ( function_exists('has_post_thumbnail') && has_post_thumbnail($post->ID) ) :
																	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), full );
																else :
																	$thumbnail=false;
																endif; ?>
																<div class="col-md-4">
																	<div class="news-box">
																		<div class="thumb"><img src="<?php echo $thumbnail[0]; ?>" alt=""></div>
																		<div class="news-caption">
																			<div class="date"><?php the_time(' F jS, Y') ?></div>
																			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h3>
																			<p><?php the_excerpt(); ?></p>

																		</div>
																	</div>
																</div>
																<?php
																endwhile;
																endif;
																?>
															</div>
															<div class="home-banner">
																<div class="row">
																	<div class="col-md-12">
																		<div class="banner-container"> <img src="<?php bloginfo("template_url")?>/images/banner-bg.png" alt="">
																			<div class="banner-caption">
																				<h3>Newborn Collection
																					Winter 2015</h3>
																					<h4>Now in store</h4>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!--Home Latest News End--> 

														<!--Partner Logo Slider-->
														<div class="partner-logos">
															<div class="plogo-slider">
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo1.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo2.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo3.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo4.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo5.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo6.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo1.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo2.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo3.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo4.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo5.jpg" alt=""></div>
																<div class="item"><img src="<?php bloginfo("template_url")?>/images/plogo6.jpg" alt=""></div>
															</div>
														</div>
														<!--Partner Logo Slider End--> 

														<!--News letter Container-->
														<div class="cp-home-newsletter">
															<div class="container">
																<div class="row">
																	<div class="col-md-12">
																		<h3>Want to hear more story,subscribe for our newsletter</h3>
																		<a class="subscribe-button" href="#">Subscribe</a> </div>
																	</div>
																</div>
															</div>

															<!--News letter Container End--> 

														</div>
														<!--Main Content End--> 


														<?php get_footer(); ?>
