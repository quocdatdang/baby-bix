<?php
/**
 * Template functions used for pages.
 *
 * @package storefront
 */

if ( ! function_exists( 'storefront_page_header' ) ) {
	/**
	 * Display the post header with a link to the single post
	 * @since 1.0.0
	 */
	function storefront_page_header() {
		?>
		<div class="inner-title">
	    <div class="container">
	      <div class="row">
	        <div class="col-md-6">
	          <?php the_title( '<h2 class="entry-title" itemprop="name">', '</h2>' ); ?>
	        </div>
	        <div class="col-md-6">
	          <ul class="breadcrumb">
	            <li><a href="<?=home_url()?>">Home</a> <a href="<?=the_permalink()?>"> <?php the_title(); ?></a></li>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </div>
		<?php
	}
}

if ( ! function_exists( 'storefront_page_content' ) ) {
	/**
	 * Display the post content with a link to the single post
	 * @since 1.0.0
	 */
	function storefront_page_content() {
		?>
		<div class="entry-content" itemprop="mainContentOfPage">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
		<?php
	}
}