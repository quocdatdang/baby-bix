<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */
?>
<!DOCTYPE html>
<html lang="en" <?php language_attributes(); ?> <?php storefront_html_tag_schema(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Child Care</title>
  <!-- Style Sheet Files -->
  <link href="<?php bloginfo("template_url")?>/css/custom.css" rel="stylesheet">
  <link href="<?php bloginfo("template_url")?>/css/bootstrap.css" rel="stylesheet">
  <link href="<?php bloginfo("template_url")?>/css/jquery.bxslider.css" rel="stylesheet">
  <link href="<?php bloginfo("template_url")?>/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php bloginfo("template_url")?>/css/color.css" rel="stylesheet">
  <link href="<?php bloginfo("template_url")?>/css/owl.carousel.css" rel="stylesheet">
  <link href="<?php bloginfo("template_url")?>/css/submenu.css" rel="stylesheet">

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

      <!--Wrapper Start-->
      <div id="child-care" class="wrapper index"> 

        <!--Header Start-->
        <header id="cp-child-header" class="cp-child-header">
          <div class="cp-child-topbar">
            <div class="container"> 
              <!--SELECT LANGUAGE START-->
              <div class="theme-language-currency">
                <p>Language:</p>
                <div class="dropdown"> <a id="language" data-toggle="dropdown" aria-expanded="true"> English <span class="caret"></span> </a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="language">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Arabic</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">German</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Spanish</a></li>
                  </ul>
                </div>
              </div>
              <!--SELECT LANGUAGE END--> 
              <!--SELECT CURRENCY START-->
              <div class="theme-language-currency">
                <p>Currency:</p>
                <div class="dropdown"> <a id="Currency" data-toggle="dropdown" aria-expanded="true"> Doller <span class="caret"></span> </a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="Currency">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Pakistan rupee</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Euro</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Dirham</a></li>
                  </ul>
                </div>
              </div>
              <!--SELECT CURRENCY END-->
              <div class="cart-menu">
                <ul>
                  <li><a href="#">My Account</a></li>
                  <li><a href="#">Shopping Cart</a></li>
                  <li><a href="#">Checkout</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="logo-nav">
            <div class="container">
              <div class="row">
                <div class="col-md-9">
                  <nav class="navbar navbar-default main-nav">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                      <a class="logo" href="#"><img src="<?php bloginfo("template_url")?>/images/bigmart.png" alt=""></a> </div>

                      <?php if ( has_nav_menu( 'main-menu' ) ) : ?>
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php
                  // Menu Index navigation menu.
                        $defaults = array(
                          'theme_location'  => '',
                          'menu'            => '',
                          'container'       => '',
                          'container_class' => '',
                          'container_id'    => '',
                          'menu_class'      => 'nav navbar-nav menu',
                          'echo'            => true,
                          'fallback_cb'     => 'wp_page_menu',
                          'before'          => '',
                          'after'           => '',
                          'link_before'     => '',
                          'link_after'      => '',
                          'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                          'depth'           => 3,
                          'walker'          => ''
                          );
                        wp_nav_menu( $defaults );
                          ?>

                
                      <?php endif; ?>
                    </div>
                  </nav>
                </div>
                <div class="col-md-3">
                  <div class="login-container">
                    <div class="cart-option">
                      <div class="dropdown"> <a id="cart" data-toggle="dropdown" aria-expanded="true"><img src="<?php bloginfo("template_url")?>/images/cart-icon.png" alt=""></a>
                        <div class="dropdown-menu" role="menu" aria-labelledby="cart">
                          <p>No Item to display</p>
                        </div>
                      </div>
                      <p>$20.00</p>
                    </div>
                    <!--LOGIN BOX START-->
                      <?php if ( is_user_logged_in() ) { ?>
                       <div class="login"> <a href="/my-account"> my </a> 
                     </div>
                    
                    <?php
                          } else {?>  
                           <div class="login"> <a href="#" data-toggle="modal" data-target="#myModal"> login </a> 
                      <!-- Modal -->
                      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                        
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Sign In</h4>
                            </div>
                            <div class="modal-body">
                              <p>
                                <input class="form-control" type="text">
                              </p>
                              <p>
                                <input class="form-control" type="password">
                              </p>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                            <?php  } ?>
 
                        
                    <!--LOGIN BOX END-->
                    <div class="search-panel">
                      <ul>
                        <li><a href="#" class="search"><img src="<?php bloginfo("template_url")?>/images/search-icon.png" alt=""></a>
                          <div class="search-field">
                            <input class="form-control" type="text" placeholder="Enter Your Keyword">
                          </div>
                        </li>
                        <li><a href="#"><img src="<?php bloginfo("template_url")?>/images/fav-icon.png" alt=""></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <!--Header End--> 


