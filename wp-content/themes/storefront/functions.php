<?php
/**
 * storefront engine room
 *
 * @package storefront
 */

/**
 * Initialize all the things.
 */
require get_template_directory() . '/inc/init.php';

/**
 * Note: Do not add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * http://codex.wordpress.org/Child_Themes
 */
 /**
 * Menu supports
 */
register_nav_menus(
    array( 
        "_menu_index"   => __("Menu Index"),
        "main-menu"     => __("Main Menu")
        ) 
    );
function create_post_type() {
  	register_post_type( 'slider',
	    array(
	      	'labels' => array(
		        'name' => __( 'Sliders' ),
		        'singular_name' => __( 'Slider' )
	      	),
	      	'show_in_menu'  => true,
	      	'menu_position' => 5,
			'public' => true,
			'has_archive' => true,
			'supports' => array(
				'title'
			)
	    )
  	);
}
add_action( 'init', 'create_post_type' );

add_filter('rwmb_meta_boxes', 'register_meta_boxes_for_slider' );

function register_meta_boxes_for_slider( $meta_boxes )
{
   // $prefix = 'nbs-';

    $meta_boxes[] = array(
        'title'    => 'Info',
        'pages'    => array( 'slider' ),
        'fields' => array(
        	array(
                'name' => 'Slogan',
                'id'   => 'slogan',
                'type' => 'text',
            ),
            array(
                'name' => 'URL',
                'id'   => 'slider-url',
                'type' => 'image_advanced',
            ),
            array(
                'name' => 'Link',
                'id'   => 'link',
                'type' => 'text',
            ),
        )
    ); 
    return $meta_boxes;
}

add_filter('pre_get_posts','get_slider');
function get_slider($query) {
  if (is_home() && $query->is_main_query ())
    $query->set ('post_type', array ('post','slider'));
    return $query;
}
function teams_post_type()
{
    $label = array(
        'name' => 'Teams', 
        'singular_name' => 'teams' 
         );
    $args = array(
        'labels' => $label, 
        'description' => 'Post type teams', 
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array( 'category', 'post_tag' ), 
        'hierarchical' => false, 
        'public' => true, 
        'show_ui' => true,
        'show_in_menu' => true, 
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true, 
        'menu_position' => 5, 
        'menu_icon' => 'dashicons-list-view', 
        'can_export' => true,
        'has_archive' => true, 
        'exclude_from_search' => false, 
        'publicly_queryable' => true, 
        'capability_type' => 'post' //
    );
 
    register_post_type('teams', $args); 
 
}
add_action('init', 'teams_post_type');
function blogs_post_type()
{
    $label = array(
        'name' => 'Blogs', 
        'singular_name' => 'Blogs' 
         );
    $args = array(
        'labels' => $label, 
        'description' => 'Post type blogs', 
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ),
        'taxonomies' => array( 'category', 'post_tag' ), 
        'hierarchical' => false, 
        'public' => true, 
        'show_ui' => true,
        'show_in_menu' => true, 
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true, 
        'menu_position' => 5, 
        'menu_icon' => 'dashicons-list-view', 
        'can_export' => true,
        'has_archive' => true, 
        'exclude_from_search' => false, 
        'publicly_queryable' => true, 
        'capability_type' => 'post' //
    );
 
    register_post_type('blogs', $args); 
 
}
add_action('init', 'blogs_post_type');

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns2',3);
if (!function_exists('loop_columns2')) {
    function loop_columns2() {
        return 3; // 3 products per row
    }
}

//edittor imager
function create_post_editor() {
    register_post_type( 'edittor',
        array(
            'labels' => array(
                'name' => __( 'Edittor' ),
                'singular_name' => __( 'Edittor' )
            ),
            'show_in_menu'  => true,
            'menu_position' => 5,
            'public' => true,
            'has_archive' => true,
            'supports' => array(
                'title'
            )
        )
    );
}
add_action( 'init', 'create_post_editor' );

add_filter('rwmb_meta_boxes', 'register_meta_boxes_for_edittor' );

function register_meta_boxes_for_edittor( $meta_boxes )
{
    $meta_boxes[] = array(
        'title'    => 'Editor',
        'pages'    => array( 'edittor' ),
        'fields' => array(
            array(
                'name' => 'image',
                'id'   => 'image',
                'type' => 'image_advanced',
            ),
        )
    ); 
    return $meta_boxes;
}

add_filter('pre_get_posts','get_edittor');
function get_edittor($query) {
  if (is_home() && $query->is_main_query ())
    $query->set ('post_type', array ('post','edittor'));
    return $query;
}
add_filter('rwmb_meta_boxes', 'register_meta_boxes_info_for_edittor' );

function register_meta_boxes_info_for_edittor( $meta_boxes )
{
    $meta_boxes[] = array(
        'title'    => 'Info',
        'pages'    => array( 'edittor' ),
        'fields' => array(
             array(
                'name' => 'Biew View',
                'id'   => 'biewview',
                'type' => 'text',
            ),
            array(
                'name' => 'Photographer',
                'id'   => 'photographer',
                'type' => 'text',
            ),
             array(
                'name' => 'Customer Name',
                'id'   => 'customername',
                'type' => 'text',
            ),
              array(
                'name' => 'Customer Email',
                'id'   => 'customeremail',
                'type' => 'text',
            ),
               array(
                'name' => 'Order Details',
                'id'   => 'orderdetails',
                'type' => 'text',
            ),
                array(
                'name' => 'Customer Bed Number',
                'id'   => 'bednumber',
                'type' => 'text',
            ),
                 array(
                'name' => 'Customer Room',
                'id'   => 'customerroom',
                'type' => 'text',
            ),
                  array(
                'name' => 'Payment',
                'id'   => 'payment',
                'type' => 'text',
            ),
        )
    ); 
    return $meta_boxes;
}

add_filter('pre_get_posts','get_edittor_info');
function get_edittor_info($query) {
  if (is_home() && $query->is_main_query ())
    $query->set ('post_type', array ('post','edittor'));
    return $query;
}




/*using file function_temp.php*/

require get_template_directory() . '/functions_temp.php';
