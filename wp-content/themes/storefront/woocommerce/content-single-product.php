<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<style type="text/css">
	.single-product div.product{overflow: visible;}
	.saletag{z-index: 999}
</style>

      <!--DETAIL PRODUCT START-->
      <div  itemscope itemtype="<?=woocommerce_get_product_schema(); ?>"  id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

        <div class="row">
          <div class="col-md-5">
            <?php
				/**
				 * woocommerce_before_single_product_summary hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
			?>
          </div>
          <!--DETAIL TEXT START-->
          <div class="col-md-7">
          <div class="text">
            <?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary');
		?>
          </div>
          </div>
          <!--DETAIL TEXT END--> 
        </div>
      </div>
      <!--DETAIL PRODUCT START-->

