<?php
/**
 * Output a single payment method
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;

}
?>

<div class="panel panel-default">
	<div class="panel-heading payment_method_<?php echo $gateway->id; ?>" role="tab" id="headingOne_<?php echo $gateway->id; ?>">
		<h4 class="panel-title"> 
			<input id="payment_method_<?php echo $gateway->id; ?>" 
				type="radio" class="input-radio" 
				name="payment_method" 
				value="<?php echo esc_attr( $gateway->id ); ?>" 
				<?php checked( $gateway->chosen, true ); ?> 
				data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" />
			<a data-toggle="collapse" 
			data-parent="#accordion" 
			href="#collapseOne_<?php echo $gateway->id; ?>" 
			aria-expanded="true" 
			aria-controls="collapseOne_<?php echo $gateway->id; ?>"> 
			<?php echo $gateway->get_title(); ?> <?php echo $gateway->get_icon(); ?> </a> 
		</h4>
    </div>
    <?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
	<div id="collapseOne_<?php echo $gateway->id; ?>" class="panel-collapse collapse payment_box payment_method_<?php echo $gateway->id; ?>" <?php if ( ! $gateway->chosen ) : ?><?php endif; ?> role="tabpanel" aria-labelledby="headingOne_<?php echo $gateway->id; ?>">
		<div class="panel-body"> <?php $gateway->payment_fields(); ?></div>
    </div>
    <?php endif; ?>
</div>
