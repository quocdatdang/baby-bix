<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<style type="text/css">
		.price del .amount{
	    color: #999;
	    text-decoration: line-through;
	    margin: 0px 16px 0px 0px;
	    font-family: 'Open Sans', sans-serif;
	}
	.price{
		    font-size: 24px;
    color: #00b7db;
    float: left;
    width: 100%;
    font-family: 'Open Sans', sans-serif;
	}
	.price ins{text-decoration: none;
		font-family: 'Open Sans', sans-serif;
	}
</style>
<div itemprop="offers" class="price">

	<div class="price"><?php echo $product->get_price_html(); ?></div>

	<meta itemprop="price" content="<?php echo $product->get_price(); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div>
