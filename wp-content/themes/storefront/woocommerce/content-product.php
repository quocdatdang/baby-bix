<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop, $post;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}
$cl  = implode(" ", $classes);
?>
<!--LIST ITEM START-->
<li  class="col-md-3 col-sm-6  <?=$cl?>" >
	<a href="<?php the_permalink(); ?>">
		<div class="pro-list">
        <?php 
        if ( $product->is_on_sale()  ) :?>

              <div class="saletag">Sale </div>
        <?php endif; ?>
              <div class="thumb">
              <a href="<?php the_permalink(); ?>">
              <?php
	              if ( has_post_thumbnail() ) {
	              	?>
	              		<img src="<?= wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
	              	<?php
        					} else{
        						?>
        	              		<img src="/image/default?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
        	              	<?php
        					}
        				?>
        			</a>
              </div>
              <div class="text">
                <div class="pro-name">
                <a href="<?php the_permalink(); ?>">
                  <h4><?php the_title(); ?></h4>
                  <p class="price"><?=$product->get_price_html(); ?></p>
                </a>
                </div>
                <p> <?php $excerpt = get_the_excerpt() ; 
                if (strlen($excerpt) > 50) {
                 	echo substr ($excerpt, 0,50).'....';
                }else{
                 	echo $excerpt;
                }
                ?>
                </p>
              </div>
              <div class="cart-options">
              	<!-- <a href="#"> <i class="fa fa-heart-o"> </i>Wishlist</a> -->
              	<?php do_action( 'woocommerce_after_shop_loop_item' );?>
                
              	<a href="<?php the_permalink(); ?>"><i class="fa fa-search"></i>Details</a> 
              	<a href="http://www.addthis.com/bookmark.php"class="addthis_button" style="text-decoration:none;"><i class="fa fa-share-alt"></i>Share
              	</a>
              </div>
        </div>
	</a>
</li>
<!--LIST ITEM END-->
