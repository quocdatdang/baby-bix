<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<!-- Get bread bar-->
<div class="inner-title">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2><?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?></h2>
        </div>
        <div class="col-md-6">
          <ul class="breadcrumb">
            <li><a href="#">Home</a> <a href="#">Shop</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!--./Get bread bar-->
  
  <!--Main Content Start-->
  <div class="cp-page-content inner-page-content woocommerce">
    <div class="container">
      <div class="home-banner">
        <div class="row">
          <div class="col-md-12">
            <div class="banner-container">
            <?php 
			  if(is_shop()){
			    $page_id = woocommerce_get_page_id('shop');
			    //$url = wp_get_attachment_thumb_url( get_post_thumbnail_id($page_id ) );
			    echo get_the_post_thumbnail($page_id );
			    ?>
	            <div class="banner-caption">
	                <?=get_post_field('post_content', $page_id);?>  
	            </div>
			<?php } ?>
			
            </div>
          </div>
        </div>
      </div>
      <?php if (is_shop()): ?>
      		<?php

			/**
			* Get product categories in shop
			*/
			 $product_categories = get_categories( apply_filters( 'woocommerce_product_subcategories_args', array(
	            'parent'       => $parent_id,
	            'menu_order'   => 'ASC',
	            'hide_empty'   => 0,
	            'hierarchical' => 1,
	            'taxonomy'     => 'product_cat',
	            'pad_counts'   => 1
	         ) ) );
			
			if ( $product_categories ) {
			echo '<ul class="nav nav-tabs" role="tablist"> <!-- Nav tabs -->';
            foreach ( $product_categories as $category ) {
            	echo '<li class="active">';
                	wc_get_template( 'content-single-cat.php', array(
                     		'category' => $category
                 		) );
            	echo '</li>';
				}
			echo '</ul>';
			}?>
      <?php else: ?>
	  <?php if ( have_posts() ) : ?>
			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php echo paginate_links(); ?>
			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				//do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>
		<?php endif;?>
	</div><!--/end container-->
</div><!--Main Content End-->

<?php get_footer( 'shop' ); ?>
