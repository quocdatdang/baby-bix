<?php
/**
 * Cart totals
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.6
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>


<div class="cart_totals <?php if ( WC()->customer->has_calculated_shipping() ) echo 'calculated_shipping'; ?>">

	<?php do_action( 'woocommerce_before_cart_totals' ); ?>
	</div>
	<div class="col-md-4">
	 <!-- TOTAL CAR START-->
	<h4>Total</h4>
	<div class="cart-steps">
		<table>
			<tr class="sub-totle">
				<td><?php _e( 'Sub Total: ', 'woocommerce' ); ?></td>
				<td><?php wc_cart_totals_subtotal_html(); ?></td>
			</tr>
			<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
			<tr class="sub-totle cart-discount coupon-<?php echo esc_attr( $code ); ?>">
				<td><?php _e( 'Coupon: ', 'woocommerce' ); ?></td>
				<td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
			</tr>
			<?php endforeach; ?>
			
			<tr class="grand-totle">
	            <td><?php _e( 'Grand Total: ', 'woocommerce' ); ?></td>
	            <td><?php wc_cart_totals_order_total_html(); ?></td>
	        </tr>
	    </table>
	    <?php  $checkout_url = WC()->cart->get_checkout_url();?>
	    <a href="<?php echo $checkout_url; ?>" class="checkout-button btn btn-style">
    	<?php  _e( 'Proceed to Checkout', 'woocommerce' ); ?></a> 
    </div>
    <!-- TOTAL CAR START-->
	</div>

    
		<?php /* foreach ( WC()->cart->get_fees() as $fee ) : ?>
			<tr class="fee">
				<th><?php echo esc_html( $fee->name ); ?></th>
				<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( WC()->cart->tax_display_cart == 'excl' ) : ?>
			<?php if ( get_option( 'woocommerce_tax_total_display' ) == 'itemized' ) : ?>
				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
					<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
						<th><?php echo esc_html( $tax->label ); ?></th>
						<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php else : ?>
				<tr class="tax-total">
					<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
					<td><?php wc_cart_totals_taxes_total_html(); ?></td>
				</tr>
			<?php endif; ?>
		<?php endif; ?>

		<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>

		<tr class="order-total">
			<th><?php _e( 'Total', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_order_total_html(); ?></td>
		</tr>

		<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>

	<?php if ( WC()->cart->get_cart_tax() ) : ?>
		<p><small><?php

			$estimated_text = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
				? sprintf( ' ' . __( ' (taxes estimated for %s)', 'woocommerce' ), WC()->countries->estimated_for_prefix() . __( WC()->countries->countries[ WC()->countries->get_base_country() ], 'woocommerce' ) )
				: '';

			printf( __( 'Note: Shipping and taxes are estimated%s and will be updated during checkout based on your billing and shipping information.', 'woocommerce' ), $estimated_text );

		?></small></p>
	<?php endif; ?>

	<div class="wc-proceed-to-checkout">
		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
	</div>

	<?php do_action( 'woocommerce_after_cart_totals' ); */ ?>

</div>
