<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

  <!--Main Content Start-->
  <div class="cp-page-content inner-page-content woocommerce">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="cart-table">
		<form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">
		<?php do_action( 'woocommerce_before_cart_table' ); ?>
        <ul>
        <?php do_action( 'woocommerce_before_cart_contents' ); ?>
         <!--TABLE CAPS START-->
              <li class="table-caps">
                <div class="product">prodct</div>
                <div class="detail">detail</div>
                <div class="price">price</div>
                <div class="quantity">Quantity</div>
                <div class="total">Total</div>
                <div class="del">Del</div>
              </li>
              <!--TABLE CAPS END--> 
		<?php
		/** START FOREACH*/
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
			<!--CART LIST START-->
			<li class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
				<div class="product">
					<?php
							$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

							if ( ! $_product->is_visible() ) {
								echo $thumbnail;
							} else {
								printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
							}
						?>
				</div>
                <div class="detail">
                  <p><?php
							if ( ! $_product->is_visible() ) {
								echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
							} else {
								echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
							}

							// Meta data
							echo WC()->cart->get_item_data( $cart_item );

							// Backorder notification
							if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
								echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
							}
						?></p>
                </div>
                <div class="price">
                  <p><?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
						?></p>
                </div>
                <?php
                	if ( $_product->is_sold_individually() ) {
                		$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
					} else {
						$product_quantity = woocommerce_quantity_input( array(
							'input_name'  => "cart[{$cart_item_key}][qty]",
							'input_value' => $cart_item['quantity'],
							'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
							'min_value'   => '0'
							), $_product, false );
						}

					echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
					?>
                <div class="total">
                 <p><?php
					echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
				?></p>
                </div>
				<div class="del product-remove">
	                <?php
	                  	echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="close remove " title="%s">&times;</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
					?>
                </div>
			</li>
			<!--CART LIST END-->
				<?php
			}
		}/** END FOREACH*/

		do_action( 'woocommerce_cart_contents' );
		?>
		</ul>
		<div class="update-cart">
			<div class="input-append">
			<?php if ( WC()->cart->coupons_enabled() ) { ?>
				<input type="text" name="coupon_code" class="input-text span2" id="appendedInputButton"  value="" placeholder="<?php _e( 'Coupon code', 'woocommerce' ); ?>" />
				<input type="submit" class="btn button" name="apply_coupon" value="<?php _e( 'Apply Coupon', 'woocommerce' ); ?>" />
					<?php do_action( 'woocommerce_cart_coupon' ); ?>
			<?php } ?>
            </div>

			<input type="submit" class="btn button pull-right" name="update_cart" value="<?php _e( 'Update Cart', 'woocommerce' ); ?>" />
        </div>
        </div>

          <div class="row">
            <div class="woocommerce-cart-options">
              	<?php do_action( 'woocommerce_cart_collaterals' ); ?>
            </div>
          </div>
		<?php do_action( 'woocommerce_after_cart_contents' ); ?>
        </div>
      </div>
    </div>
  </div>
  <!--Main Content End--> 