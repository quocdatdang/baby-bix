<?php 
$site_url = site_url();
$category_slug = get_option('woocommerce_product_category_slug') ? get_option('woocommerce_product_category_slug') : _x( 'product-category', 'slug', 'woocommerce' );
$category_archive_url = $site_url . '/' . $category_slug . '/' . $category->slug;
?>
<a href="<?=esc_url($category_archive_url)?>" id="cate-item-<?=$category->term_id?>" class="cate-name-shop cate-item-<?=$category->term_id?>"><?=$category->name?></a>

<?php wp_reset_query();
		//Check exist of product in cate
			$args = array( 'post_type' => 'product', 
				'posts_per_page' => 4, 
				'tax_query'     => array(
			        array(
			            'taxonomy'  => 'product_cat',
			            'field'     => 'id', 
			            'terms'     => $category->term_id
			        )
			    ),
			 'orderby' => 'rand' );
			query_posts($args); ?>
			<?php if ( have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php

			endif;
			// Reset Query
			wp_reset_query();
		?>