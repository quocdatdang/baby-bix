<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var addthis_product = $('.addthis_toolbox:first').remove();
		 $('.addthis_toolbox').remove();
		$('.col-md-7 .text').append(addthis_product);
	});
</script>

  <!--Main Content Start-->
  <div class="cp-page-content inner-page-content woocommerce">
    <div class="container">
      <div class="home-banner">
        <div class="row">
          <div class="col-md-12">
            
          </div>
        </div>
      </div>
.
		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>
			<?php
			global $product, $post; 
			$term_list = wp_get_post_terms($post->ID,'product_cat',array('fields'=>'ids'));
			$list_cate = implode(',',$term_list);
		?>	
		<?php endwhile; // end of the loop. ?>




<div class="product-similar">
<br/>
	<h2>Sẩn phẩm tương tự</h2>
	<br/><br/>
</div>
		<?php 
		wp_reset_query();
		//Check exist of product in cate
		if(isset($list_cate)){
			$args = array( 'post_type' => 'product', 
				'posts_per_page' => 4, 
				'tax_query'     => array(
			        array(
			            'taxonomy'  => 'product_cat',
			            'field'     => 'id', 
			            'terms'     => $term_list
			        )
			    ),
			 'orderby' => 'rand' );
			query_posts($args);
			//$loop = new WP_Query( $args );
			// The Loop?>
			<?php if ( have_posts() ) : ?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php

			endif;
			// Reset Query
			wp_reset_query();
		}
		?>
</div>
</div>

<?php get_footer( 'shop' ); ?>
