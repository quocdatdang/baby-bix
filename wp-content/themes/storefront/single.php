<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */

get_header(); ?>

	 <!--  <div class="inner-title">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2>Blog Detail Page With Sidebar</h2>
        </div>
        <div class="col-md-6">
          <ul class="breadcrumb">
            <li><a href="#">Home</a> <a href="#">Blog</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div> -->

		<?php while ( have_posts() ) : the_post(); ?>

 <div class="cp-page-content inner-page-content blog-posts blog-with-sidebar">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="blog-post">
			<?php
			/*do_action( 'storefront_single_post_before' );*/

			get_template_part( 'content', 'single' );

			/**
			 * @hooked storefront_post_nav - 10
			 */
			do_action( 'storefront_single_post_after' );
			?>

		<?php endwhile; // end of the loop. ?>

		<!--  -->

    <aside class="col-md-3">
         <?php do_action( 'storefront_sidebar' ); ?>
        </aside>
  </div>
</div>
</div>

<?php get_footer(); ?>