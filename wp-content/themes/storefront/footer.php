<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
?>
<!--Footer Start-->
  <div id="footer" class="cp-footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="widget_text">
              <h3>Babypic </h3>
              <p>BABYPIC được thành lập với sứ mệnh lưu giữ khoảnh khắc đặc biệt của em bé khi chào đời. Đây sẽ là kỷ vật vô giá theo con bạn suốt cuộc đời hoặc sẽ là món quà đặc biệt dành tặng cho người thân yêu</p>
              <a href="http://localhost/babybix/welcome-to-babypic/" class="f-readmore">Read More</a> </div>
          </div>
          <div class="col-md-3">
            <div class="widget_text">
              <h3>Contact Us</h3>
              <address>
              <p>135 Street 24, Binh Tri Dong Ward, Binh Tan Dis., HCMC  </p>
              <p>(08)3 762 0872<br>
                0927 751 759</p>
             <!--  <p><a href="mailto:Info@babypic.com">Info@bab.com</a></p> -->
              </address>
            </div>
          </div>
          <div class="col-md-3">
            <div class="widget_text">
              <h3>Opening Time</h3>
              <p>Monday-Friday: ______8.00 to 18.00<br>
                Saturday: ____________ 9.00 to 18.00<br>
                Sunday: _____________10.00 to 16.00</p>
              <p>Every 30 day of month Lorem ipsum
                dolor sit amet, adipiscing elit.</p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="pro-slider">
              <h3>On Sale</h3>
              <ul class="footer-pro-slider">
                <li>
                  <div class="thumb"><img src="<?php bloginfo("template_url")?>/images/fpro-slider.jpg" alt="">
                    <div class="thumb-caption"> <span class="dis-tag">65% Off</span> <span class="cart-like"> <a href="#"><i class="fa fa-shopping-cart"></i></a> <a href="#"><i class="fa fa-heart-o"></i></a> </span>
                      <div class="text">
                        <p>Lorem ipsum dolor sit amet</p>
                        <del>$750</del><ins> $600</ins> </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="thumb"><img src="<?php bloginfo("template_url")?>/images/fpro-slider2.jpg" alt="">
                    <div class="thumb-caption"> <span class="dis-tag">65% Off</span> <span class="cart-like"> <a href="#"><i class="fa fa-shopping-cart"></i></a> <a href="#"><i class="fa fa-heart-o"></i></a> </span>
                      <div class="text">
                        <p>Lorem ipsum dolor sit amet</p>
                        <del>$750</del><ins> $600</ins> </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="thumb"><img src="<?php bloginfo("template_url")?>/images/fpro-slider.jpg" alt="">
                    <div class="thumb-caption"> <span class="dis-tag">65% Off</span> <span class="cart-like"> <a href="#"><i class="fa fa-shopping-cart"></i></a> <a href="#"><i class="fa fa-heart-o"></i></a> </span>
                      <div class="text">
                        <p>Lorem ipsum dolor sit amet</p>
                        <del>$750</del><ins> $600</ins> </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p>Copyright 2015. All Rights Reserved.</p>
          </div>
          <div class="col-md-6">
            <div class="footer-social"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-pinterest"></i></a> <a href="#"><i class="fa fa-dribbble"></i></a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Footer End--> 
  
</div>
<!--Wrapper End--> 
<!-- JS Files --> 
<script src="<?php bloginfo("template_url")?>/js/jquery.min.js"></script><!-- Jquery File --> 
<script src="<?php bloginfo("template_url")?>/js/jquery-migrate-1.2.1.min.js"></script> <!-- Jquery Migrate --> 
<script src="<?php bloginfo("template_url")?>/js/bootstrap.min.js"></script> <!-- Bootstrap --> 
<script src="<?php bloginfo("template_url")?>/js/jquery.easing.1.3.js"></script> <!-- easing --> 
<script src="<?php bloginfo("template_url")?>/js/jquery.bxslider.min.js"></script> <!-- Bx Slider --> 
<script src="<?php bloginfo("template_url")?>/js/owl.carousel.min.js"></script> <!-- easing --> 
<script src="<?php bloginfo("template_url")?>/js/custom.js"></script> <!-- easing -->
<?php wp_footer(); ?>

</body>
</html>