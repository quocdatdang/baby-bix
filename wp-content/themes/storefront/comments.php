<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package storefront
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<style type="text/css">
input[type="email"] ,input[type="url"] {
	background: #fff;
  border-radius: 5px;
  border: 1px solid #cdcdcd;
  padding: 10px 20px;
  height: auto;
  margin: 0px;
}
input[type="url"] {
	width: 100%;
}
</style>

<div class="post-comments-form">
            <h3>Leave a Comment</h3>
            <div class="row"><ul class="comment-form">
	<?php if ( have_comments() ) : ?>
		

		

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'      	=> 'ol',
					'short_ping' 	=> true,
					'callback'		=> 'storefront_comment',
				) );
			?>
		</ol><!-- .comment-list -->

		

	<?php endif; // have_comments() ?>

	<?php if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'storefront' ); ?></p>
	<?php endif; ?>

	<div class="lvcomment"><?php comment_form(); ?></div>
</ul>

</div>
          </div> 
      </div>
  </div>