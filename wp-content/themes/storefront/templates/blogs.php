<?php
/**
* The template for dislaying all pages
*
*Template Name: Blogs
*/

get_header(); ?>
<div class="inner-title">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2>Tin tức</h2>
			</div>
			<div class="col-md-6">
				<ul class="breadcrumb">
					<li><a href="#">Trang chủ</a> <a href="#">Tin tức</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!--Main Content Start-->
<div class="cp-page-content inner-page-content blog-posts">
	<div class="container">
		<div class="row">
			<?php $new_query = new WP_Query( 'post_type=blogs' );
			?>
			<?php 
			if ( $new_query->have_posts() ) : 
				$thumbnail=false;
			while( $new_query->have_posts() ) :
				$new_query->the_post();
			if ( function_exists('has_post_thumbnail') && has_post_thumbnail($post->ID) ) :
				$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), full );
			else :
				$thumbnail=false;
			endif; ?>
			<div class="blog-post">
				<div class="col-md-3">
					<div class="post-tools">
						<div class="post-date"><i class="fa fa-calendar"></i> <strong><?php the_time(' F jS, Y') ?></strong></div>
						<h4 class="user">by admin</h4>
						<div class="post-tags"> <a href="#">children</a>, <a href="#">toys</a>, <a href="#">baby sitter</a>, <a href="#">prams</a>, <a href="#">bornbaby cloths</a></div>
						<a href="<?php the_permalink(); ?>" class="readmore-blue">Read More <i class="fa fa-caret-right"></i></a> </div>
					</div>
					<div class="col-md-9">
						<div class="post-thumb">
							<ul class="cp-about-slider">
								<li><img src="<?php echo $thumbnail[0]; ?>"/></li>
							</ul>
						</div>
						<h3><?php the_title(); ?></h3>
						<p><?php the_excerpt(); ?></p>
					</div>
				</div>
				<?php
				endwhile;
				endif;
				?>
			</div>
		</div>
		<!--Main Content End--> 

		<!--News letter Container-->
		<div class="cp-home-newsletter">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3>Want to hear more story,subscribe for our newsletter</h3>
						<a class="subscribe-button" href="#">Subscribe</a> </div>
					</div>
				</div>
			</div>

			<!--News letter Container End--> 


			<?php get_footer(); ?>
