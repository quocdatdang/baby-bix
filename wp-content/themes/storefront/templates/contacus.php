<?php
/**
* The template for dislaying all pages
*
*Template Name: Contact us
*/

get_header(); ?>
<style type="text/css">
input{
	height: 40px;
	width: 100%;
	border: 1px solid #efefef
}
textarea{
	width: 100%;
	border: 1px solid #efefef
}
input[type="submit"]{
	width: 152px;
	height: 47px;
	background: url(http://localhost/babybix/wp-content/themes/storefront/images/bluebtn.png);
	border: none;
}
input[type="submit"]:hover{
	width: 152px;
	height: 47px;
	background: url(http://localhost/babybix/wp-content/themes/storefront/images/bluebtn.png);
}
</style>
<div class="inner-title">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2>Liên hệ</h2>
        </div>
        <div class="col-md-6">
          <ul class="breadcrumb">
            <li><a href="#">Trang chủ</a> <a href="#"></a></li>Liên hệ
          </ul>
        </div>
      </div>
    </div>
  </div>
  
  <!--Main Content Start-->
  <div class="cp-page-content inner-page-content">
    <div class="container">
      <div class="contact-us">
        <div class="row">
          <div class="col-md-6">
            <h2>Babypic</h2>
            <div class="contact-form">
              <h4>Contact</h4>

		 	<?php echo do_shortcode('[contact-form-7 id="20" title="Contact form 1"]');?>
              
            </div>
          </div>
          <div class="col-md-6">
            <h2>Opening Hours</h2>
            <div class="opening-hours">
             <!--  <ul>
                <li>Monday  -  Friday <strong>9am  -  5pm</strong></li>
                <li>Saturday <strong>9am  -  3pm</strong></li>
                <li>Sunday <strong>9am  -  12pm</strong></li>
              </ul> -->
            </div>
            <div class="contact-form">
              <ul class="cinfo">
                <li> Phone      (08)3 762 0872 - 0927 751 759 </li>
                <li> Adress      135 Street 24, Binh Tri Dong Ward, Binh Tan Dis., HCMC </li>
                <li> Website      www.babypic.net </li>
              </ul>
            </div>
          </div>
          
          <div class="col-md-12">
          <div class="map">
          
          <div id="map_canvas" class="map_canvas">
          	<iframe src="http://static.parastorage.com/services/santa/1.792.2/static/external/googleMap.html?address=135%20%C4%91%C6%B0%E1%BB%9Dng%2024%2C%20B%C3%ACnh%20Tr%E1%BB%8B%20%C4%90%C3%B4ng%20B%2C%20B%C3%ACnh%20T%C3%A2n%2C%20H%E1%BB%93%20Ch%C3%AD%20Minh&amp;addressInfo=BABYPIC&amp;lat=10.7541738&amp;long=106.612122&amp;mapInteractive=true&amp;mapType=ROADMAP&amp;showMapType=true&amp;showPosition=true&amp;showStreetView=true&amp;showZoom=true&amp;ts=1076" width="100%" height="100%" frameborder="0" scrolling="no" ></iframe>
          </div>
          
          </div>
          
          </div>
          
          
        </div>
      </div>
    </div>
  </div>
  <!--Main Content End--> 
  
  <!--Partner Logo Slider--> 
  
  <!--Partner Logo Slider End--> 
  <!--News letter Container-->
  <div class="cp-home-newsletter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3>Want to hear more story,subscribe for our newsletter</h3>
          <a class="subscribe-button" href="#">Subscribe</a> </div>
      </div>
    </div>
  </div>
  
  <!--News letter Container End--> 

<?php get_footer(); ?>