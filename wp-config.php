<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_baby');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bD)W=5:;Jas`z6sE;@SjGz)s^.`k65*K,+ B()w-5e-?ID{?_jdDkMO,kaj?m1_;');
define('SECURE_AUTH_KEY',  'POE,qtNoj5>u^Wn+]2sOB^X4Qe^+NTh6-|j><&VM.+@Fxa( mbfNQ|(CG;pD:G1]');
define('LOGGED_IN_KEY',    '@H[P(]Sa@t;-ba#Tj4-{*:k!~fw>(I(;VO*XyM3>zju&vljj`5(*^s-wK=NR5YSz');
define('NONCE_KEY',        'I`OkX~ w+SY?_RqbCLKi^t!dIX/j-cs;39m:}j-=d-@URkgfp`ePx@D!gzM;x!]@');
define('AUTH_SALT',        ' ed;e8qL/4a>ayf+7(sWWUYY<)oetcCn0]{B#qzP`vp?Na26DNHbPw|h,PE[g %m');
define('SECURE_AUTH_SALT', '$]+y9:0k.Gce{ZMcxTSM#Uwy0?*_i%l$tZlDACnpOZ:XY$#InAo/K|mN$D6]<Q +');
define('LOGGED_IN_SALT',   'cB~_i5)4 z6yH=4_t+(uZHUi;Kx/`0hb2+L|;!#(L4u#kxFgb$( f1g&*JVjk^Pn');
define('NONCE_SALT',       '.2btWzq3?n-{n9 cK$sih%/g3W7Gv$ K!wK7<O%0e%o%476*w{cp 0=NMPVSe:2i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'baby_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
